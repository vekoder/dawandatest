package rs.vekoder.dawandatest;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import rs.vekoder.dawandatest.retrofit.DaWandaRetrofitClient;

/**
 * @author velibor.gacina on 10/18/2017.
 */

public class DaWandaApp extends MultiDexApplication {

    private static DaWandaApp mInstance;

    private static DaWandaRetrofitClient daWandaRetrofitClient = null;

    private static Repository repository = null;

    public static synchronized DaWandaApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public DaWandaRetrofitClient getDaWandaRetrofitClient() {
        if (daWandaRetrofitClient == null) {
            daWandaRetrofitClient = new DaWandaRetrofitClient();
        }
        return daWandaRetrofitClient;
    }

    public Repository getRepository() {
        if (repository == null) {
            repository = new Repository();
        }

        return repository;
    }
}

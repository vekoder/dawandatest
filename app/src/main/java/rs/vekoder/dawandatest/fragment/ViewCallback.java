package rs.vekoder.dawandatest.fragment;

import java.util.List;

/**
 * @author velibor.gacina on 10/21/2017.
 */

public interface ViewCallback<T> {

    void onDataLoaded(List<T> products);

    void onLoading();

    void onFailedToLoadData(String errorMessage);

}

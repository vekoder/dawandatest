package rs.vekoder.dawandatest.fragment;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import butterknife.BindView;
import butterknife.Unbinder;
import rs.vekoder.dawandatest.R;

/**
 * @author velibor.gacina on 10/21/2017.
 */

public abstract class BaseFragment extends Fragment {

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    protected Unbinder unbinder;

    protected abstract boolean shouldDisplayHomeAsUp();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(shouldDisplayHomeAsUp());
        } else {
            Log.w(this.getClass().getSimpleName(), "Action Bar is not supported.");
        }
    }
}

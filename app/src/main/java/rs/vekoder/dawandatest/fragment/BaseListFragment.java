package rs.vekoder.dawandatest.fragment;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import rs.vekoder.dawandatest.MainViewCallback;
import rs.vekoder.dawandatest.R;

/**
 * @author velibor.gacina on 10/21/2017.
 */

public abstract class BaseListFragment<T> extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, ViewCallback<T> {

    @BindView(R.id.swipe_container)
    protected SwipeRefreshLayout swipeContainer;

    @BindView(R.id.pb_loading)
    protected ProgressBar progressBar;

    @BindView(R.id.tv_no_response)
    protected TextView tvNoResponse;

    protected MainViewCallback parent;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parent = (MainViewCallback) context;
    }

    protected void setLoadingUIState() {
        tvNoResponse.setVisibility(View.GONE);
        if (!swipeContainer.isRefreshing()) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    protected void setNoDataUIState() {
        tvNoResponse.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        hideRefreshSpinner();
    }

    protected void setNormalUIState() {
        tvNoResponse.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);

        hideRefreshSpinner();
    }

    private void hideRefreshSpinner() {
        if (swipeContainer.isRefreshing()) {
            swipeContainer.setRefreshing(false);
        }
    }


}

package rs.vekoder.dawandatest;

import rs.vekoder.dawandatest.products.model.Product;

/**
 * @author by velibor.gacina on 10/18/2017.
 */

public interface MainViewCallback {

    void onCategorySelected(int id, String name);

    void onProductSelected(Product product);
}

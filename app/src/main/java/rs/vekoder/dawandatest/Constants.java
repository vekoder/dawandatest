package rs.vekoder.dawandatest;

/**
 * @author velibor.gacina on 10/19/2017.
 */

final public class Constants {

    public static final String KEY_CATEGORY_ID = "category_id";

    public static final String KEY_CATEGORY_NAME = "category_name";

    public static final String KEY_PRODUCT_ID = "product_id";

    public static final String KEY_PRODUCT_TITLE = "product_title";
}

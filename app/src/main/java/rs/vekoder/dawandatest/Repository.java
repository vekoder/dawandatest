package rs.vekoder.dawandatest;

import java.util.List;

import rs.vekoder.dawandatest.products.model.Product;

/**
 * @author velibor.gacina on 10/20/2017.
 */

public class Repository {

    private List<Product> loadedProducts;

    public void setLoadedProducts(List<Product> loadedProducts) {
        this.loadedProducts = loadedProducts;
    }

    public Product getProductById(int id) {
        if (loadedProducts == null || loadedProducts.isEmpty()) {
            return null;
        }
        for (Product p : loadedProducts) {
            if (p.getId() == id) {
                return p;
            }
        }
        return null;
    }

}

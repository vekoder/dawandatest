package rs.vekoder.dawandatest.products;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rs.vekoder.dawandatest.R;
import rs.vekoder.dawandatest.products.model.Product;

/**
 * @author velibor.gacina on 10/20/2017.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private List<Product> items;

    private Context context;

    ProductsAdapter(Context context, List<Product> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_product, parent, false);
        return new ProductsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Product product = items.get(position);
        holder.title.setText(product.getTitle());
        holder.price.setText(context.getString(R.string.price) + "  " + product.getPrice().getDisplayable());
        String url = product.getDefaultImage().getListviewL();
        //url = "https://api.learn2crack.com/android/images/donut.png";
        Picasso.with(context).load("http:" + url).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(List<Product> data) {
        items = data;
    }

    public Product getItem(int position) {
        return items.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_prodcard_title)
        TextView title;
        @BindView(R.id.tv_prodcard_price)
        TextView price;
        @BindView(R.id.iv_prodcard_image)
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

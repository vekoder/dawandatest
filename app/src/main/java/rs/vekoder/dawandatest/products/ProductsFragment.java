package rs.vekoder.dawandatest.products;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rs.vekoder.dawandatest.Constants;
import rs.vekoder.dawandatest.DaWandaApp;
import rs.vekoder.dawandatest.R;
import rs.vekoder.dawandatest.fragment.BaseListFragment;
import rs.vekoder.dawandatest.products.model.Product;
import rs.vekoder.dawandatest.recyclerview.RecyclerItemDecoration;
import rs.vekoder.dawandatest.recyclerview.RecyclerItemListener;

/**
 * @author velibor.gacina on 10/18/2017.
 */

public class ProductsFragment extends BaseListFragment<Product> {

    public static final String TAG = "ProductsFragment";

    @BindView(R.id.recv_prodfra)
    RecyclerView recyclerView;

    private int categoryId;

    private String categoryName;

    private ProductsPresenter presenter;

    public static ProductsFragment newInstance() {
        return new ProductsFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        unbinder = ButterKnife.bind(this, view);

        categoryId = getArguments().getInt(Constants.KEY_CATEGORY_ID);
        categoryName = getArguments().getString(Constants.KEY_CATEGORY_NAME);

        if (toolbar != null) {
            toolbar.setTitle(categoryName);
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        }
        setHasOptionsMenu(true);

        swipeContainer.setOnRefreshListener(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new RecyclerItemDecoration(getResources().getDimensionPixelSize(R.dimen.product_margin_vertical)));
        setItemClickListener();

        presenter = new ProductsPresenter(this);
        presenter.loadData(categoryId);

        return view;
    }

    @Override
    protected boolean shouldDisplayHomeAsUp() {
        return true;
    }

    @Override
    public void onDataLoaded(List<Product> products) {
        setNormalUIState();
        recyclerView.setVisibility(View.VISIBLE);

        DaWandaApp.getInstance().getRepository().setLoadedProducts(products);
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter == null) {
            adapter = new ProductsAdapter(getActivity(), products);
            recyclerView.setAdapter(adapter);
        } else {
            ((ProductsAdapter) adapter).setData(products);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoading() {
        recyclerView.setVisibility(View.GONE);
        setLoadingUIState();
    }

    @Override
    public void onRefresh() {
        presenter.loadData(categoryId);
    }

    @Override
    public void onFailedToLoadData(String errorMessage) {
        Log.e(TAG, "Fail to load data: " + errorMessage);
        setNoDataUIState();
        recyclerView.setVisibility(View.GONE);
    }

    private void setItemClickListener() {
        recyclerView.addOnItemTouchListener(new RecyclerItemListener(getActivity(), new RecyclerItemListener.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ProductsAdapter productsAdapter = (ProductsAdapter) recyclerView.getAdapter();
                Product item = productsAdapter.getItem(position);
                ProductsFragment.this.parent.onProductSelected(item);
            }
        }));
    }
}

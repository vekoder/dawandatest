package rs.vekoder.dawandatest.products;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import rs.vekoder.dawandatest.DaWandaApp;
import rs.vekoder.dawandatest.fragment.ViewCallback;
import rs.vekoder.dawandatest.products.model.Product;
import rs.vekoder.dawandatest.retrofit.DaWandaService;

/**
 * @author velibor.gacina on 10/20/2017.
 */

class ProductsPresenter {

    private ViewCallback<Product> productsView;

    private DaWandaService daWandaService;


    ProductsPresenter(ViewCallback<Product> productsView) {
        this.productsView = productsView;
        daWandaService = DaWandaApp.getInstance().getDaWandaRetrofitClient().getDaWandaService();
    }

    void loadData(int categoryId) {
        if (daWandaService == null) {
            productsView.onFailedToLoadData("DaWandaService not available");
            return;
        }

        daWandaService.getProductsRx(categoryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<List<Product>>() {
                    @Override
                    public void onNext(@NonNull List<Product> products) {
                        productsView.onDataLoaded(products);

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        productsView.onFailedToLoadData(e.getMessage());

                    }

                    @Override
                    public void onComplete() {
                        //do nothing
                    }
                });

    }

}


package rs.vekoder.dawandatest.products.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("milli_units_per_item")
    @Expose
    private Integer milliUnitsPerItem;
    @SerializedName("unit")
    @Expose
    private Object unit;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("price")
    @Expose
    private Price price;
    @SerializedName("undiscounted_price")
    @Expose
    private UndiscountedPrice undiscountedPrice;
    @SerializedName("base_price")
    @Expose
    private BasePrice basePrice;
    @SerializedName("badge")
    @Expose
    private String badge;
    @SerializedName("sale_percentage")
    @Expose
    private Object salePercentage;
    @SerializedName("discounted")
    @Expose
    private Boolean discounted;
    @SerializedName("sold_in_unit")
    @Expose
    private Boolean soldInUnit;
    @SerializedName("seller")
    @Expose
    private Seller seller;
    @SerializedName("shop")
    @Expose
    private Shop shop;
    @SerializedName("default_image")
    @Expose
    private DefaultImage defaultImage;
    @SerializedName("pinned")
    @Expose
    private Boolean pinned;
    @SerializedName("customizable")
    @Expose
    private Boolean customizable;
    @SerializedName("campaigned")
    @Expose
    private Boolean campaigned;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Integer getMilliUnitsPerItem() {
        return milliUnitsPerItem;
    }

    public void setMilliUnitsPerItem(Integer milliUnitsPerItem) {
        this.milliUnitsPerItem = milliUnitsPerItem;
    }

    public Object getUnit() {
        return unit;
    }

    public void setUnit(Object unit) {
        this.unit = unit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public UndiscountedPrice getUndiscountedPrice() {
        return undiscountedPrice;
    }

    public void setUndiscountedPrice(UndiscountedPrice undiscountedPrice) {
        this.undiscountedPrice = undiscountedPrice;
    }

    public BasePrice getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BasePrice basePrice) {
        this.basePrice = basePrice;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public Object getSalePercentage() {
        return salePercentage;
    }

    public void setSalePercentage(Object salePercentage) {
        this.salePercentage = salePercentage;
    }

    public boolean getDiscounted() {
        return discounted;
    }

    public void setDiscounted(boolean discounted) {
        this.discounted = discounted;
    }

    public Boolean getSoldInUnit() {
        return soldInUnit;
    }

    public void setSoldInUnit(Boolean soldInUnit) {
        this.soldInUnit = soldInUnit;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public DefaultImage getDefaultImage() {
        return defaultImage;
    }

    public void setDefaultImage(DefaultImage defaultImage) {
        this.defaultImage = defaultImage;
    }

    public Boolean getPinned() {
        return pinned;
    }

    public void setPinned(Boolean pinned) {
        this.pinned = pinned;
    }

    public Boolean getCustomizable() {
        return customizable;
    }

    public void setCustomizable(Boolean customizable) {
        this.customizable = customizable;
    }

    public Boolean getCampaigned() {
        return campaigned;
    }

    public void setCampaigned(Boolean campaigned) {
        this.campaigned = campaigned;
    }

}

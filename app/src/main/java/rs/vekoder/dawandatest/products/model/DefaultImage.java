
package rs.vekoder.dawandatest.products.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DefaultImage {

    @SerializedName("big")
    @Expose
    private String big;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("long")
    @Expose
    private String _long;
    @SerializedName("product_l")
    @Expose
    private String productL;
    @SerializedName("full")
    @Expose
    private String full;
    @SerializedName("listview")
    @Expose
    private String listview;
    @SerializedName("listview_xs")
    @Expose
    private String listviewXs;
    @SerializedName("listview_s")
    @Expose
    private String listviewS;
    @SerializedName("listview_m")
    @Expose
    private String listviewM;
    @SerializedName("listview_l")
    @Expose
    private String listviewL;
    @SerializedName("pin")
    @Expose
    private String pin;
    @SerializedName("square_130")
    @Expose
    private String square130;

    public String getBig() {
        return big;
    }

    public void setBig(String big) {
        this.big = big;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }

    public String getProductL() {
        return productL;
    }

    public void setProductL(String productL) {
        this.productL = productL;
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public String getListview() {
        return listview;
    }

    public void setListview(String listview) {
        this.listview = listview;
    }

    public String getListviewXs() {
        return listviewXs;
    }

    public void setListviewXs(String listviewXs) {
        this.listviewXs = listviewXs;
    }

    public String getListviewS() {
        return listviewS;
    }

    public void setListviewS(String listviewS) {
        this.listviewS = listviewS;
    }

    public String getListviewM() {
        return listviewM;
    }

    public void setListviewM(String listviewM) {
        this.listviewM = listviewM;
    }

    public String getListviewL() {
        return listviewL;
    }

    public void setListviewL(String listviewL) {
        this.listviewL = listviewL;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSquare130() {
        return square130;
    }

    public void setSquare130(String square130) {
        this.square130 = square130;
    }

}

package rs.vekoder.dawandatest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import butterknife.ButterKnife;
import rs.vekoder.dawandatest.categories.CategoriesFragment;
import rs.vekoder.dawandatest.productdetail.ProductDetailFragment;
import rs.vekoder.dawandatest.products.ProductsFragment;
import rs.vekoder.dawandatest.products.model.Product;

/**
 * @author velibor.gacina on 10/18/2017.
 */
public class MainActivity extends AppCompatActivity implements MainViewCallback {

    private static final String TAG = "MainActivity";

    private enum EFragment {
        eCategory(CategoriesFragment.TAG),
        eProducts(ProductsFragment.TAG),
        eDetail(ProductDetailFragment.TAG);

        String tag;

        EFragment(String tag) {
            this.tag = tag;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            setFragment(EFragment.eCategory, null);
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCategorySelected(int id, String name) {
        Bundle data = new Bundle();
        data.putInt(Constants.KEY_CATEGORY_ID, id);
        data.putString(Constants.KEY_CATEGORY_NAME, name);
        setFragment(EFragment.eProducts, data);
    }

    @Override
    public void onProductSelected(Product product) {
        Bundle data = new Bundle();
        data.putInt(Constants.KEY_PRODUCT_ID, product.getId());
        data.putString(Constants.KEY_PRODUCT_TITLE, product.getTitle());
        setFragment(EFragment.eDetail, data);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    void setFragment(EFragment fragmentType, Bundle data) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentType.tag);

        if (fragment == null) {
            fragment = createFragment(fragmentType);
            if (data != null) {
                fragment.setArguments(data);
            }
        }

        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                .replace(R.id.fl_main, fragment, fragmentType.tag);

        if (fragmentType != EFragment.eCategory) {
            transaction.addToBackStack(fragmentType.tag);

        }
        transaction.commit();

    }

    private Fragment createFragment(EFragment fragmentType) {
        switch (fragmentType) {
            case eCategory:
                return CategoriesFragment.newInstance();
            case eProducts:
                return ProductsFragment.newInstance();
            case eDetail:
                return ProductDetailFragment.newInstance();
            default:
                return CategoriesFragment.newInstance();
        }
    }

}

package rs.vekoder.dawandatest.categories;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rs.vekoder.dawandatest.R;
import rs.vekoder.dawandatest.fragment.BaseListFragment;
import rs.vekoder.dawandatest.recyclerview.RecyclerItemDecoration;
import rs.vekoder.dawandatest.recyclerview.RecyclerItemListener;

/**
 * @author velibor.gacina on 10/18/2017.
 */

public class CategoriesFragment extends BaseListFragment<Category> {

    public static final String TAG = "CategoriesFragment";

    @BindView(R.id.recv_catfra)
    RecyclerView recyclerView;

    private CategoriesPresenter categoriesPresenter;


    public static CategoriesFragment newInstance() {
        return new CategoriesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (toolbar != null) {
            toolbar.setTitle(getString(R.string.categories_name));
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        }
        setHasOptionsMenu(true);

        swipeContainer.setOnRefreshListener(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addItemDecoration(new RecyclerItemDecoration(getResources().getDimensionPixelSize(R.dimen.category_margin_vertical)));
        setItemClickListener();

        categoriesPresenter = new CategoriesPresenter(this);
        categoriesPresenter.loadData();


        return view;
    }

    @Override
    protected boolean shouldDisplayHomeAsUp() {
        return false;
    }

    @Override
    public void onDataLoaded(List<Category> data) {
        setNormalUIState();
        recyclerView.setVisibility(View.VISIBLE);

        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter == null) {
            adapter = new CategoriesAdapter(data, getActivity());
            recyclerView.setAdapter(adapter);
        } else {
            ((CategoriesAdapter) adapter).setData(data);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailedToLoadData(String errorMessage) {
        Log.e(TAG, "Fail to load data. " + errorMessage);
        setNoDataUIState();
        recyclerView.setVisibility(View.GONE);

    }

    @Override
    public void onLoading() {
        recyclerView.setVisibility(View.GONE);
        setLoadingUIState();
    }

    @Override
    public void onRefresh() {
        categoriesPresenter.loadData();
    }

    private void setItemClickListener() {
        recyclerView.addOnItemTouchListener(new RecyclerItemListener(getActivity(), new RecyclerItemListener.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                CategoriesAdapter categoriesAdapter = (CategoriesAdapter) recyclerView.getAdapter();
                Category item = categoriesAdapter.getItem(position);
                CategoriesFragment.this.parent.onCategorySelected(item.getId(), item.getName());
            }
        }));
    }


}

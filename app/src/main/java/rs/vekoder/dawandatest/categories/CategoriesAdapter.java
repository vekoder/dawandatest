package rs.vekoder.dawandatest.categories;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import rs.vekoder.dawandatest.R;

/**
 * @author velibor.gacina on 10/18/2017.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private List<Category> categories;

    private Context context;

    public CategoriesAdapter(List<Category> data, Context context) {
        this.categories = data;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(categories.get(position).getName());
        String url = categories.get(position).getImageUrl();
        //url = "https://api.learn2crack.com/android/images/donut.png";
        Picasso.with(context).load("http:" + url).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public Category getItem(int position) {
        return categories.get(position);
    }

    public void setData(List<Category> data) {
        categories = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_cardcateg_name);
            image = (ImageView) itemView.findViewById(R.id.iv_cardcateg_image);
        }
    }
}

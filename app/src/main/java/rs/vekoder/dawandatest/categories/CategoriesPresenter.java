package rs.vekoder.dawandatest.categories;

import android.util.Log;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import rs.vekoder.dawandatest.DaWandaApp;
import rs.vekoder.dawandatest.fragment.ViewCallback;
import rs.vekoder.dawandatest.retrofit.DaWandaRetrofitClient;

/**
 * @author velibor.gacina on 10/19/2017.
 */

class CategoriesPresenter {
    private static final String TAG = "CategoriesPresenter";

    private ViewCallback<Category> view;

    private DaWandaRetrofitClient retrofitClient;

    CategoriesPresenter(ViewCallback<Category> view) {
        this.view = view;
        retrofitClient = DaWandaApp.getInstance().getDaWandaRetrofitClient();
    }

    void loadData() {
        view.onLoading();
        retrofitClient
                .getDaWandaService()
                .getCategoriesRx()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<List<Category>>() {
                    @Override
                    public void onNext(@NonNull List<Category> categories) {
                        view.onDataLoaded(categories);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.onFailedToLoadData(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete()");
                    }
                });

    }

}

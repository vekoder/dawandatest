package rs.vekoder.dawandatest.productdetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import rs.vekoder.dawandatest.Constants;
import rs.vekoder.dawandatest.DaWandaApp;
import rs.vekoder.dawandatest.R;
import rs.vekoder.dawandatest.fragment.BaseFragment;
import rs.vekoder.dawandatest.products.model.Product;

/**
 * @author velibor.gacina on 10/20/2017.
 */

public class ProductDetailFragment extends BaseFragment {

    public static final String TAG = "ProductDetailFragment";

    @BindView(R.id.iv_prodetfra_image)
    ImageView image;
    @BindView(R.id.tv_prodetfra_title)
    TextView title;
    @BindView(R.id.tv_prodetfra_price)
    TextView price;

    private Product product;

    public static ProductDetailFragment newInstance() {
        return new ProductDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        unbinder = ButterKnife.bind(this, view);

        int productId = getArguments().getInt(Constants.KEY_PRODUCT_ID);

        product = DaWandaApp.getInstance().getRepository().getProductById(productId);

        if (toolbar != null) {
            toolbar.setTitle(product.getTitle());
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        }

        Picasso.with(getActivity()).load("http:" + product.getDefaultImage().getBig()).into(image);
        title.setText(product.getTitle());
        price.setText(product.getPrice().getDisplayable());

        return view;
    }

    @Override
    protected boolean shouldDisplayHomeAsUp() {
        return true;
    }
}

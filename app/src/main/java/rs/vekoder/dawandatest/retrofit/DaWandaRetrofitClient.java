package rs.vekoder.dawandatest.retrofit;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author velibor.gacina on 10/19/2017.
 */

public class DaWandaRetrofitClient {

    private static final String HOST_URL = "https://de.dawanda.com/third-party-public/";

    private DaWandaService daWandaService;

    public DaWandaRetrofitClient() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HOST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        daWandaService = retrofit.create(DaWandaService.class);
    }

    public DaWandaService getDaWandaService() {
        return daWandaService;
    }
}

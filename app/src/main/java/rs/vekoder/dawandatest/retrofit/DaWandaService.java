package rs.vekoder.dawandatest.retrofit;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import rs.vekoder.dawandatest.categories.Category;
import rs.vekoder.dawandatest.products.model.Product;

/**
 * @author velibor.gacina on 10/19/2017.
 */

public interface DaWandaService {

    @Headers("Content-Type: application/json")
    @GET("categories.json")
    Observable<List<Category>> getCategoriesRx();

    @Headers("Content-Type: application/json")
    @GET("categories.json")
    Call<List<Category>> getCategoriesCall();

    @Headers("Content-Type: application/json")
    @GET("categories/{id}.json")
    Observable<List<Product>> getProductsRx(@Path("id") int categoryId);
}
